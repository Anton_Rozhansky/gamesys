package home.rao.task.gamesys.util;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class PropertyResolver {

    private static Properties props = null;

    private PropertyResolver() {
    }

    public static String getString(String property) {
        if (props == null) {
            props = initProps();
        }
        return props.getProperty(property);
    }

    public static int getInt(String property) {
        String stringValue = getString(property)
                .trim()
                .replace(" ", "")
                .replace("_", "");
        return Integer.parseInt(stringValue);
    }

    public static Properties initProps() {
        Properties prop = null;
        try (InputStream input = PropertyResolver
                .class.getClassLoader().getResourceAsStream("config.properties")) {
            if (input == null) {
                throw new ExceptionInInitializerError("Unable to find config.properties");
            }
            prop = new Properties();
            prop.load(input);
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        return prop;
    }
}
