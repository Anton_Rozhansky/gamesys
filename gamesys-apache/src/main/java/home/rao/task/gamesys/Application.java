package home.rao.task.gamesys;

import home.rao.task.gamesys.config.TestConfig;
import home.rao.task.gamesys.config.TestConfigFactory;
import home.rao.task.gamesys.core.Executor;

public class Application {

    public static void main(String[] args) {
        Application application = new Application();
        application.launchTest();
    }

    private void launchTest() {
        TestConfig testConfig = TestConfigFactory.getConfig();
        Executor executor = new Executor(testConfig);
        executor.run();
    }
}
