package home.rao.task.gamesys.core;

import home.rao.task.gamesys.config.TestConfig;
import home.rao.task.gamesys.report.RequestResult;
import org.apache.http.client.config.RequestConfig;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class Executor {

    private final TestConfig testConfig;
    private RequestConfig requestConfig;
    private List<HttpGetRequest> requests;

    public Executor(TestConfig testConfig) {
        this.testConfig = testConfig;
    }

    public void run() {
        configureRequestConfig();
        initRequests();
        Map<String, Long> report = requests.stream()
                .parallel()
                .map(HttpGetRequest::execute)
                .collect(Collectors.groupingBy(RequestResult::getDescription, Collectors.counting()));

        System.out.println(report);
    }

    private void configureRequestConfig() {
        requestConfig = RequestConfig.custom()
                .setRedirectsEnabled(true)
                .setRelativeRedirectsAllowed(true)
                .build();
    }

    private void initRequests() {
        requests = new ArrayList<>();
        for (int i = 0; i < testConfig.getRequestNumber(); i++) {
            requests.add(new HttpGetRequest(testConfig.getAim(), requestConfig));
        }
    }
}
