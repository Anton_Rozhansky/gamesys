package home.rao.task.gamesys.config;

import home.rao.task.gamesys.util.PropertyResolver;

public class TestConfigFactory {

    public static TestConfig getConfig() {
        TestConfig config = new TestConfig();
        config.setAim(PropertyResolver.getString("aim"));
        config.setRequestNumber(PropertyResolver.getInt("number"));
        return config;
    }

    private TestConfigFactory() {}
}
