package home.rao.task.gamesys.config;

public class TestConfig {

    private String aim;
    private int requestNumber;

    public String getAim() {
        return aim;
    }

    void setAim(String aim) {
        this.aim = aim;
    }

    public int getRequestNumber() {
        return requestNumber;
    }

    void setRequestNumber(int requestNumber) {
        this.requestNumber = requestNumber;
    }
}
