package home.rao.task.gamesys.core;

import home.rao.task.gamesys.report.RequestResult;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;

import java.io.IOException;

public class HttpGetRequest {

    private final HttpGet request;

    public HttpGetRequest(String url, RequestConfig config) {
        this.request = new HttpGet(url);
        request.setConfig(config);
    }

    RequestResult execute() {
        RequestResult requestResult = new RequestResult();
        try (CloseableHttpClient httpClient = HttpClients.createDefault();
             CloseableHttpResponse response = httpClient.execute(request)) {
            requestResult.setStatus(response.getStatusLine().getStatusCode());
            requestResult.setDescription(response.getStatusLine().getReasonPhrase());
        } catch (IOException exception) {
            exception.printStackTrace();
        }
        return requestResult;
    }
}
